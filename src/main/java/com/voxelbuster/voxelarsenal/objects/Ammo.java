package com.voxelbuster.voxelarsenal.objects;

import com.google.gson.Gson;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Ammo {
    private final String id, displayName, itemId;

    private final boolean explosive, explosionDestroysBlocks;
    private final float explosionPower;

    private transient Material item;

    public Ammo(String id, String displayName, Material item) {
        this(id, displayName, item, false, false, 0f);
    }

    public Ammo(String id, String displayName, Material item, boolean explosive, boolean explosionDestroysBlocks,
                float explosionPower) {
        this.id = id;
        this.displayName = displayName;
        this.item = item;
        this.itemId = item.getKey().getNamespace() + ":" + item.getKey().getKey();
        this.explosive = explosive;
        this.explosionDestroysBlocks = explosionDestroysBlocks;
        this.explosionPower = explosionPower;
    }

    public static Ammo fromConfig(File jsonFile) throws IOException {
        Gson gson = new Gson();
        FileReader fr = new FileReader(jsonFile);
        Ammo obj = gson.fromJson(fr, Ammo.class);
        fr.close();
        obj.item = Material.getMaterial(obj.itemId);
        return obj;
    }

    public ItemStack getItemStack() {
        return getItemStack(1);
    }

    public ItemStack getItemStack(int amount) {
        ItemStack is = new ItemStack(item, amount);
        PersistentDataContainer pdc = is.getItemMeta().getPersistentDataContainer();
        pdc.set(ItemDataUtil.ammoTypeKey, PersistentDataType.STRING, id);
        return is;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getId() {
        return id;
    }

    public boolean isExplosive() {
        return explosive;
    }

    public boolean doesExplosionDestroyBlocks() {
        return explosionDestroysBlocks;
    }

    public float getExplosionPower() {
        return explosionPower;
    }

    public void toConfig(File jsonFile) throws IOException {
        Gson gson = new Gson();
        FileWriter fw = new FileWriter(jsonFile);
        gson.toJson(this, fw);
        fw.close();
    }
}
