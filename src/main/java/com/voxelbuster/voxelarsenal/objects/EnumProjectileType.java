package com.voxelbuster.voxelarsenal.objects;

public enum EnumProjectileType {
    HITSCAN,
    BULLET,
    ROCKET
}
