package com.voxelbuster.voxelarsenal.objects;

public enum EnumWeaponAttachmentType {
    OPTIC,
    BARREL,
    STABILITY,
    AMMO,
    OTHER
}
