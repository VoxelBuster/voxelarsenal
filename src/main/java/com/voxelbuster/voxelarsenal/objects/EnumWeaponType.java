package com.voxelbuster.voxelarsenal.objects;

public enum EnumWeaponType {
    PISTOL,
    REVOLVER,
    SHOTGUN,
    BOLT_SNIPER,
    SEMI_AUTO_SNIPER,
    ASSAULT_RIFLE,
    SMG,
    LMG,
    GRENADE_LAUNCHER,
    ROCKET_LAUNCHER
}
