package com.voxelbuster.voxelarsenal.commands;

import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import org.bukkit.command.CommandSender;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.bukkit.ChatColor.*;

public class SubcommandHelp extends PluginSubcommand {
    protected SubcommandHelp(VoxelArsenalPlugin plugin) {
        super("help", plugin);
        this.description = "Show this help message.";
        this.usage = "/va help" + YELLOW + " [subcommand]";
        this.aliases = Arrays.asList("help", "h");
    }

    @Override
    public boolean execute(CommandSender commandSender, String alias, String[] args) {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length < 1) {
                commandSender.sendMessage(new String[]{
                        AQUA + "/va help",
                        AQUA + "/va weapon " + GREEN + "<list/give/reload/setAmmo> " + GOLD + "[options]",
                        AQUA + "/va ammo " + GREEN + "<list/give/weapons> " + GOLD + "[options]",
                        AQUA + "/va reload " + WHITE + " : Admin Only",
                        AQUA + "/va config " + GREEN + "<key> <value>" + WHITE + " : Admin Only",
                        });
            } else {
                PluginBaseCommand.SubCommand subCommandType = PluginBaseCommand.SubCommand.subCommandByAlias(args[0]);
                if (subCommandType == null) {
                    commandSender.sendMessage(RED + "No such subcommand.");
                    return false;
                }

                StringBuilder aliasesSb = new StringBuilder();
                Iterator<String> aliasesIterator = aliases.iterator();
                while (aliasesIterator.hasNext()) {
                    aliasesSb.append(GREEN).append(aliasesIterator.next()).append(GOLD).append(", ");
                }
                aliasesSb.append(aliasesIterator.next());

                PluginSubcommand subcommand;
                try {
                    subcommand = subCommandType.getSubcommand().getConstructor(VoxelArsenalPlugin.class)
                            .newInstance(plugin);
                    commandSender.sendMessage(GOLD + "Help for " + GREEN + subcommand.name);
                    commandSender.sendMessage(WHITE + subcommand.description);
                    commandSender.sendMessage(GOLD + "Aliases: " + aliasesSb.toString());
                    commandSender.sendMessage(GOLD + "Usage: " + GREEN + subcommand.usage);
                } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                    plugin.getLogger().severe("Failed to reflectively construct subcommands!");
                    e.printStackTrace();
                }
                return true;
            }
        }
        sendUsageMessage(commandSender);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length > 0) {
                return Arrays.asList((String[]) PluginBaseCommand.SubCommand.getAllAliases().stream()
                        .filter(s -> s.startsWith(args[0].toLowerCase())).toArray());
            } else {
                return Collections.emptyList();
            }
        } else {
            return null;
        }
    }

}
