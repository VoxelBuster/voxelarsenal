package com.voxelbuster.voxelarsenal.commands;

import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.List;

public abstract class PluginSubcommand {
    protected final String name;
    protected final VoxelArsenalPlugin plugin;

    protected String description, usage;
    protected List<String> aliases;

    protected PluginSubcommand(String name, VoxelArsenalPlugin plugin) {
        this.name = name;
        this.plugin = plugin;
    }

    public abstract boolean execute(CommandSender commandSender, String alias, String[] args);

    public abstract List<String> tabComplete(CommandSender sender, String alias,
                                             String[] args) throws IllegalArgumentException;

    public String getDescription() {
        return description;
    }

    public String getUsage() {
        return usage;
    }

    public String getName() {
        return name;
    }

    public void sendUsageMessage(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "Usage: " + ChatColor.GREEN + usage);
    }

    public List<String> getAliases() {
        return aliases;
    }
}
