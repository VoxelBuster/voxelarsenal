package com.voxelbuster.voxelarsenal;

import com.voxelbuster.voxelarsenal.objects.Ammo;
import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Set;

public class WeaponManager {
    private static WeaponManager instance;

    private final HashMap<String, Weapon> weaponsMap = new HashMap<>();
    private final HashMap<String, Ammo> ammoMap = new HashMap<>();
    private final VoxelArsenalPlugin plugin;

    protected WeaponManager(@NotNull VoxelArsenalPlugin plugin) {
        this.plugin = plugin;
        WeaponManager.instance = this;
    }

    public static WeaponManager getInstance() {
        return instance;
    }

    public void loadWeapons(@NotNull String weaponsFolderPath) throws IOException {
        weaponsMap.clear();
        Files.list(Paths.get(weaponsFolderPath)).forEach(path -> {
            try {
                File f = path.toFile();
                if (f.isFile()) {
                    Weapon w = Weapon.fromConfig(f);
                    weaponsMap.put(w.getId(), w);
                }
            } catch (IOException e) {
                plugin.getLogger().severe(String.format("Error loading weapons config (%s):", path.toString()));
                e.printStackTrace();
            }
        });
    }

    public void writeWeapons(@NotNull String weaponsFolderPath) throws IOException {
        for (Weapon w : weaponsMap.values()) {
            File f = Paths.get(weaponsFolderPath, w.getId() + ".json").toFile();
            w.toConfig(f);
        }
    }

    public Weapon[] getAllWeapons() {
        return weaponsMap.values().toArray(new Weapon[0]);
    }

    public Weapon getWeaponById(String id) {
        return weaponsMap.get(id);
    }

    public @NotNull Set<String> getWeaponIds() {
        return weaponsMap.keySet();
    }

    public void clearWeapons() {
        weaponsMap.clear();
    }

    public void addWeapon(String id, Weapon w) {
        weaponsMap.putIfAbsent(id, w);
    }

    public void loadAmmo(@NotNull String ammoFolderPath) throws IOException {
        ammoMap.clear();
        Files.list(Paths.get(ammoFolderPath)).forEach(path -> {
            try {
                File f = path.toFile();
                if (f.isFile()) {
                    Ammo a = Ammo.fromConfig(f);
                    ammoMap.put(a.getId(), a);
                }
            } catch (IOException e) {
                plugin.getLogger().severe(String.format("Error loading ammo config (%s):", path.toString()));
                e.printStackTrace();
            }
        });
    }

    public void writeAmmo(@NotNull String ammoFolderPath) throws IOException {
        for (Ammo a : ammoMap.values()) {
            File f = Paths.get(ammoFolderPath, a.getId() + ".json").toFile();
            a.toConfig(f);
        }
    }

    public Ammo[] getAllAmmo() {
        return ammoMap.values().toArray(new Ammo[0]);
    }

    public Ammo getAmmoById(String id) {
        return ammoMap.get(id);
    }

    public @NotNull Set<String> getAmmoIds() {
        return ammoMap.keySet();
    }

    public void clearAmmo() {
        ammoMap.clear();
    }

    public void addAmmo(String id, Ammo a) {
        ammoMap.putIfAbsent(id, a);
    }
}