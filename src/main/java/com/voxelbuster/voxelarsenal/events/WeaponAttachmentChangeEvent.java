package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import com.voxelbuster.voxelarsenal.objects.WeaponAttachment;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;

/**
 * Not yet used.
 */
public class WeaponAttachmentChangeEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Weapon weapon;
    private final List<WeaponAttachment> oldAttachments;
    private final List<WeaponAttachment> currentAttachments;

    public WeaponAttachmentChangeEvent(@NotNull Weapon weapon,
                                       @NotNull List<WeaponAttachment> oldAttachments,
                                       @NotNull List<WeaponAttachment> currentAttachments) {
        this.weapon = weapon;
        this.oldAttachments = oldAttachments;
        this.currentAttachments = currentAttachments;
    }

    @NotNull
    public Weapon getWeapon() {
        return weapon;
    }

    @NotNull
    public List<WeaponAttachment> getOldAttachments() {
        return Collections.unmodifiableList(oldAttachments);
    }

    @NotNull
    public List<WeaponAttachment> getCurrentAttachments() {
        return currentAttachments;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
