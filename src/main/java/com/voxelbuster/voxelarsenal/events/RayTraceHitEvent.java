package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.RayTraceResult;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Fires when an entity is hit by a weapon's hitscan.
 */
public class RayTraceHitEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Entity entityHit;
    private final Weapon weaponFired;
    private final double damageDealt;
    private final Player player;

    public RayTraceHitEvent(@Nullable Player player, @NotNull Entity entityHit,
                            @Nullable Weapon weaponFired, double damageDealt) {
        this.player = player;
        this.entityHit = entityHit;
        this.weaponFired = weaponFired;
        this.damageDealt = damageDealt;
    }

    /**
     * Returns the entity hit by the hitscan.
     */
    @NotNull
    public Entity getEntityHit() {
        return entityHit;
    }

    /**
     * Gets the weapon that produced the hitscan.
     */
    @Nullable
    public Weapon getWeaponFired() {
        return weaponFired;
    }

    /**
     * Gets the damage dealt by the weapon.
     */
    public double getDamageDealt() {
        return damageDealt;
    }

    public Player getPlayer() {
        return player;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
