package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.VoxelArsenalPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.IOException;

@SuppressWarnings("unused")
public class GlobalEventHandler implements Listener {
    private final VoxelArsenalPlugin plugin;

    public GlobalEventHandler(VoxelArsenalPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        try {
            plugin.getConfigManager().loadPlayerData(player, plugin.getPlayersDir());
        } catch (IOException e) {
            plugin.getLogger().severe(String.format("Failed to load player %s! They will be reset to the defaults!",
                    player.getName()));
            e.printStackTrace();
        }
    }
}
