package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * Called when a player reloads (shift-rightclick) a Weapon.
 */
public class WeaponReloadEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private final Weapon weapon;

    public WeaponReloadEvent(Player player, @NotNull Weapon weapon) {
        this.player = player;
        this.weapon = weapon;
    }

    /**
     * Gets the player that triggered this event.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets the Weapon that triggered this event.
     */
    @NotNull
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
