package com.voxelbuster.voxelarsenal.events;

import com.voxelbuster.voxelarsenal.objects.Weapon;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Fires when a Weapon runs out of ammo. Also fires if a player attempts to fire a Weapon that is empty.
 */
public class WeaponOutOfAmmoEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final Weapon weapon;
    private final Player player;

    public WeaponOutOfAmmoEvent(@NotNull Weapon weapon, @Nullable Player player) {
        this.weapon = weapon;
        this.player = player;
    }

    /**
     * Gets the weapon that caused this event.
     */
    @NotNull
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Gets the Player that caused this event.
     */
    @Nullable
    public Player getPlayer() {
        return player;
    }

    /**
     * Gets a list of Handlers for this event.
     */
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }
}
