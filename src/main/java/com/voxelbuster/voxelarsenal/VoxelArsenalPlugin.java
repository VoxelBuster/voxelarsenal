package com.voxelbuster.voxelarsenal;

import com.voxelbuster.voxelarsenal.build.PluginSetupUtil;
import com.voxelbuster.voxelarsenal.commands.PluginBaseCommand;
import com.voxelbuster.voxelarsenal.dynamic.effects.SmokeTrailEffect;
import com.voxelbuster.voxelarsenal.events.GlobalEventHandler;
import com.voxelbuster.voxelarsenal.events.WeaponEventHandler;
import de.slikey.effectlib.EffectManager;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class VoxelArsenalPlugin extends JavaPlugin {

    private static final PluginManager pluginManager = Bukkit.getPluginManager();
    private static VoxelArsenalPlugin instance;
    private static Economy econ = null;
    private static Permission perms = null;
    private static Chat chat = null;

    private final Logger log = this.getLogger();
    private final ConfigManager configManager = new ConfigManager(this);
    private final WeaponManager weaponManager = new WeaponManager(this);
    private final String configFilename = String.valueOf(Paths.get(getDataFolder().getPath(), "config.json"));
    private final File configFile = new File(configFilename);
    private final String playersDirname = String.valueOf(Paths.get(getDataFolder().getPath(), "playerdata"));
    private final File playersDir = new File(playersDirname);
    private final String weaponsDirname = String.valueOf(Paths.get(getDataFolder().getPath(), "assets/weapons"));
    private final String ammoDirname = String.valueOf(Paths.get(getDataFolder().getPath(), "assets/ammo"));
    private final EffectManager em;

    static {
        VoxelArsenalPlugin.class.getClassLoader().setDefaultAssertionStatus(true);
    }

    public VoxelArsenalPlugin() {
        instance = this;
        this.em = new EffectManager(this);
    }

    @NotNull
    public static VoxelArsenalPlugin getInstance() {
        return instance;
    }

    @NotNull
    public EffectManager getEffectManager() {
        return em;
    }

    /*public static EffectLib getEffectLib() {
        Plugin effectLib = Bukkit.getPluginManager().getPlugin("EffectLib");
        if (!(effectLib instanceof EffectLib)) {
            return null;
        }
        return (EffectLib) effectLib;
    }*/

    public void reload() {
        try {
            log.info("Saving players...");
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.severe("Failed to save player data!");
            e.printStackTrace();
        }


        try {
            log.info("Loading config...");
            configManager.loadConfig(configFile);
        } catch (IOException e) {
            log.severe("Config failed to load! Falling back to default!");
            e.printStackTrace();
        }

        loadOnlinePlayers();

        try {
            weaponManager.loadWeapons(weaponsDirname);
            weaponManager.loadAmmo(ammoDirname);
        } catch (IOException e) {
            log.severe("Error loading weapons and ammo configs!");
            e.printStackTrace();
        }
    }

    public void loadOnlinePlayers() {
        getServer().getOnlinePlayers().forEach(player -> {
            try {
                configManager.loadPlayerData(player, playersDir);
            } catch (IOException e) {
                log.severe(String.format("Failed to load player %s! They will be reset to the defaults!",
                        player.getName()));
                e.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void onDisable() {
        try {
            log.info("Saving players...");
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.severe("Failed to save player data!");
            e.printStackTrace();
        }
        super.onDisable();
    }

    @Override
    public void onEnable() {
        try {
            log.info("Loading config...");
            configManager.loadConfig(configFile);
        } catch (IOException e) {
            log.severe("Config failed to load! Falling back to default!");
            e.printStackTrace();
        }

        validateResources();

        if (setupEconomy()) {
            log.info("Vault API Hooked.");
        } else {
            log.warning("Vault not found, features that require economy will not work.");
        }

        if (setupPermissions()) {
            PermissionUtil.setPermissionManager(perms);
        }

        setupChat();

        PermissionUtil.registerPermissions();

        pluginManager.registerEvents(new WeaponEventHandler(this), this);
        pluginManager.registerEvents(new GlobalEventHandler(this), this);

        PluginBaseCommand pbc = new PluginBaseCommand(this);
        PluginCommand c = this.getCommand("voxelarsenal");

        assert c != null; // This should never fail in production

        c.setExecutor(pbc);
        c.setTabCompleter(pbc);

        em.registerEffectClass("smokeTrail", SmokeTrailEffect.class);

        try {
            weaponManager.loadWeapons(weaponsDirname);
            weaponManager.loadAmmo(ammoDirname);
        } catch (IOException e) {
            log.severe("Error loading weapons and ammo configs!");
            e.printStackTrace();
        }
    }

    private void validateResources() {
        if (!Paths.get(getDataFolder().toPath().toString(), "assets", "weapons").toFile().isDirectory() ||
                !Paths.get(getDataFolder().getPath(), "assets", "ammo").toFile().isDirectory()) {
            if (Paths.get(getDataFolder().getPath(), ".genResources").toFile().isFile()) {
                log.warning(".genResources file found in plugin directory. " +
                        "Default resources will be generated and then the server will be stopped.");
                log.warning("This functionality is only meant for build & test. If you would like to use the " +
                        "default resources, please delete your /plugins/VoxelArsenal/weapons and ammo folders!");
                PluginSetupUtil.generateResources();
                boolean success = Paths.get(getDataFolder().getPath(), ".genResources").toFile().delete();
                if (!success) {
                    log.severe("Failed to delete .genResources file! Please delete this file as it will keep" +
                            "the server in a bootloop if it cannot be deleted!");
                }
                log.warning("Shutting down the server.");
                getServer().shutdown();
            }
            PluginSetupUtil.copyResources();
        }
    }

    private boolean setupEconomy() {
        if (pluginManager.getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return true;
    }

    private boolean setupPermissions() {
        if (pluginManager.getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        if (rsp == null) {
            return false;
        }
        perms = rsp.getProvider();
        return true;
    }

    private boolean setupChat() {
        if (pluginManager.getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        if (rsp == null) {
            return false;
        }
        chat = rsp.getProvider();
        return true;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public File getPlayersDir() {
        return playersDir;
    }

    public String getWeaponsDirname() {
        return weaponsDirname;
    }

    public String getAmmoDirname() {
        return ammoDirname;
    }

    public WeaponManager getWeaponManager() {
        return weaponManager;
    }
}
