#!/usr/bin/env bash
echo "Deploying plugins from ./target"

TARGET_DIR=./target
PLUGINS_DIR=./test_server/plugins
EFFECTLIB_URL=https://media.forgecdn.net/files/2794/64/EffectLib-6.2.jar

mkdir "$PLUGINS_DIR"

echo "Copying VoxelArsenal pluging from $TARGET_DIR"
cp "$TARGET_DIR/voxelarsenal-"*.jar "$PLUGINS_DIR"

if test ! -f "$PLUGINS_DIR/EffectLib.jar"; then
  wget -O "$PLUGINS_DIR/EffectLib.jar" "$EFFECTLIB_URL"
fi

echo "Done"