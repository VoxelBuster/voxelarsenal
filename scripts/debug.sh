#!/usr/bin/env bash
echo "Starting Spigot Test Server in Debug Mode"
java -Xms512m -Xmx4g -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=100 -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:G1MixedGCLiveThresholdPercent=35 -XX:+AlwaysPreTouch -XX:+ParallelRefProcEnabled -XX:ParallelGCThreads=4 -jar paper.jar nogui
echo "Terminated with code: $?"